package com.peters.snke.ormtest;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{
    EditText editText;
    Button button;
    ListView listView;
    List<testdata> liste;
    DatabaseHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helper=new DatabaseHelper(getApplicationContext());//getappplicationcontext
        editText= (EditText)findViewById(R.id.editText);
        button = (Button)findViewById(R.id.button);
        button.setOnClickListener((android.view.View.OnClickListener) this);
        listView=(ListView)findViewById(R.id.listView);
        liste= new ArrayList<testdata>();
        ListAdapter adapter = new ArrayAdapter<testdata>
                (getApplicationContext(), android.R.layout.simple_list_item_1, liste);
        listView.setAdapter(adapter);

    }


    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.button){
            testdata test = new testdata();
                    test.setTests(editText.getText().toString());
            helper.addData(test);
        liste=helper.GetData();
        }
    }
}
